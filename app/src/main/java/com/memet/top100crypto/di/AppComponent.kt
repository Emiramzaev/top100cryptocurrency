package com.memet.top100crypto.di

import com.memet.top100crypto.activities.ChartActivity
import com.memet.top100crypto.activities.MainActivity
import com.memet.top100crypto.chart.LatestChart
import com.memet.top100crypto.fragments.CurrenciesListFragment
import com.memet.top100crypto.mvp.contract.presenter.CurrenciesPresenter
import com.memet.top100crypto.mvp.contract.presenter.LatestChartPresenter
import dagger.Component

import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class, RestModule::class, MvpModule::class, ChartModule::class))
@Singleton
interface AppComponent {

    fun inject(mainActivity: MainActivity)
    fun inject(presenter: CurrenciesPresenter)
    fun inject(presenter: LatestChartPresenter)
    fun inject(fragment: CurrenciesListFragment)
    fun inject(chart: LatestChart)
    fun inject(activity: ChartActivity)
}